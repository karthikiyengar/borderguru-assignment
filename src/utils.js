module.exports = {
  csvToJson: (string, cols) => string.split('\r\n')
    .map(line => line.split(', ')
      .reduce((acc, value, index) => {
        acc[cols[index]] = value;
        return acc;
      }, {}))
}