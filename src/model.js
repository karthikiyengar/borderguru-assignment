class Model {
  constructor (data) {
    this._data = data;
  }
  /**
   * Find elements which match the given object
   * @param Object criteria 
   * @return {[Object]}
   */
  find(criteria) {
    const keys = Object.keys(criteria)
    if (keys.length === 0) {
      return this._data
    }
    return this._data.filter(item => {
      let valid = false;
      for (const key of keys) {
        if (item.hasOwnProperty(key) && item[key] === criteria[key]) {
          valid = true
        } else {
          return false;
        };
      }
      return valid;
    })
  }
  /**
   * Remove a single element
   * @param {String|Number} id 
   */
  remove(criteria) {
    const keys = Object.keys(criteria)
    const length = this._data.length;
    if (keys.length === 0) {
      return false;
    }
    this._data = this._data.filter(item => {
      let remove = true;
      for (const key of keys) {
        if (item.hasOwnProperty(key) && item[key] === criteria[key]) {
          remove = false
        } else {
          return true;
        };
      }
      return remove;
    })
    return length - this._data.length;
  }
  /**
   * Group by a column name and return the count
   * @param String column 
   */
  groupAndCount(column) {
    return this._data.reduce((acc, value) => {
      const exists = acc.find((result) => result.value === value[column])
      if (exists) {
        exists.count = ++exists.count;
        return acc;
      }
      acc.push({ value: value[column], count: 1 })
      return acc;
    }, [])
  }
}

module.exports = Model;