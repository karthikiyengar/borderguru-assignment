const fs = require('fs');
const path = require('path');
const utils = require('./utils');
const Model = require('./model');

const text = fs.readFileSync(path.join(__dirname, './data/sample.txt'));
const json = utils.csvToJson(text.toString(), ['orderId', 'companyName', 'customerAddress', 'orderedItem']);

const orders = new Model(json);

/**
 * show all orders for a particular company
 */
function one() {
  return orders.find({ companyName: 'SuperTrader' })
}

/**
 * show all orders to a particular address
 */
function two() {
  return orders.find({ customerAddress: 'Steindamm 80' })
}

/**
 * delete a particular order given an OrderId
 */
function three() {
  orders.remove({ orderId: '008' });
  return orders.find({});
}

/**
 * display how often each item has been ordered, in descending order
 */
function four() {
  return orders.groupAndCount('orderedItem')
    .sort((a, b) => b.count - a.count)
}

// all orders for SuperTrader
console.log(one()); 

// all orders to Steindamm 80
console.log(two()); 

// remove OrderId 001
console.log(three()); 

// count and sort ordered items
console.log(four()); 