This project was built in Node v7.9.0.

Install Dependencies

```npm install```

Executing the project

```npm start```

Please visit index.js and comment/uncomment the desired outputs

For development

```npm run dev```

Run Tests

```npm run test```
