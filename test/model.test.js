const expect = require('chai').expect;
const Model = require('../src/model');

const data = [{
  id: 1,
  name: 'Will',
  age: 26
}, {
  id: 2,
  name: 'Bob',
  age: 51
}, {
  id: 3,
  name: 'Jeff',
  age: 51
}];
let model;

describe('model.find', () => {
  beforeEach(() => {
    model = new Model(data.slice());
  })
  it('should work for a single criteria', () => {
    expect(model.find({ age: 26 }))
      .to.deep.equal([{
        id: 1,
        name: 'Will',
        age: 26
      }])
  })
  it('should return all results if no criteria', () => {
    expect(model.find({}))
      .to.deep.equal(data)
  })
  it('should work for multiple criteria', () => {
    expect(model.find({ name: 'Bob', age: 26 }))
      .to.deep.equal([]);
    expect(model.find({ name: 'Will', age: 26 }))
      .to.deep.equal([{
        id: 1,
        name: 'Will',
        age: 26
      }])
  })
})

describe('model.remove', () => {
  beforeEach(() => {
    model = new Model(data.slice());
  })
  it('should remove an item with a single criteria', () => {
    const removedCount = model.remove({ id: 1 });
    expect(removedCount).to.equal(1);
    expect(model.find({}))
      .to.deep.equal(data.slice(1))
  })
  it('should remove an item with a multiple criteria', () => {
    const removedCount = model.remove({ age: 51 });
    expect(removedCount).to.equal(2);
    expect(model.find({}))
      .to.deep.equal(data.slice(0, 1))
    expect(model.remove({ name: 'Bob', age: 99 }))
      .to.equal(0);
  })
  it('should do nothing when criteria', () => {
    model.remove({ id: 25 });
    expect(model._data)
      .to.deep.equal(data)
  })
})

describe('model.groupAndCount', () => {
  beforeEach(() => {
    model = new Model(data.slice());
  })
  it ('should return a count for the specified column', () => {
    expect(model.groupAndCount('age'))
      .to.deep.equal([{
        value: 26,
        count: 1
      }, {
        value: 51,
        count: 2
      }])
  })
})