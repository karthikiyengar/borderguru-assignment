const expect = require('chai').expect;
const utils = require('../src/utils');

describe('utils', () => {
  it('should parse one line into json', () => {
    expect(utils.csvToJson('John, Doe, Macbook', ['firstName', 'lastName', 'item']))
      .to.deep.equal([{
        firstName: 'John',
        lastName: 'Doe',
        item: 'Macbook'
      }]);
  });
  it('should should work for multiple lines', () => {
    expect(utils.csvToJson('Johnny, Cash, Guitar\r\nJohn, Doe, Outlook', ['firstName', 'lastName', 'item']))
      .to.deep.equal([{
        firstName: 'Johnny',
        lastName: 'Cash',
        item: 'Guitar'
      }, {
        firstName: 'John',
        lastName: 'Doe',
        item: 'Outlook'
      }]);
  });
})